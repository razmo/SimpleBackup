#!/bin/bash

DSTDIR="/mnt/hdd1/synced/Обмен"
LOGFILE="/var/log/backup/Obmen.log"

cd /mnt/hdd1/reglament
./inc-backup.sh -e obmen-list.conf -o 32 10.10.10.10::obmen ${DSTDIR} >> "${LOGFILE}" 2>&1 

if [ "$?" -eq "0" ]; then
	catalog=$(find "${DSTDIR}" -mindepth 1 -maxdepth 1 -type d -exec stat --format '%Y %n' "{}" \; | sort -nr | awk 'NR==1 {print $2}')
	setfacl -m u:zabbix:rx  "${catalog}"
	setfacl -m u:zabbix:r "${DSTDIR}/latest/backup.txt"
	echo "Set permission for user zabbix"
else
	echo "Error synched Обмен" >> "${LOGFILE}"
fi

