#!/bin/bash

DSTDIR="/mnt/hdd1/synced/Media"
LOGFILE="/var/log/backup/Media.log"

cd "$(dirname "$0")"

./inc-backup.sh -e media-list.conf -o 63 10.10.10.10::obmen ${DSTDIR} >> ${LOGFILE} 2>&1

if [ "$?" -eq "0" ]; then
	catalog=$(find "${DSTDIR}" -mindepth 1 -maxdepth 1 -type d -exec stat --format '%Y %n' "{}" \; | sort -nr | awk 'NR==1 {print $2}')
	setfacl -m u:zabbix:rx "${catalog}"
	setfacl -m u:zabbix:r "${DSTDIR}/latest/backup.txt"
	echo "Set permissions for user zabbix" >> ${LOGFILE}
else
	echo "Error synched Media" >> ${LOGFILE}
fi

