#!/bin/bash

# если переменная не определена генерировать ошибку
set -o nounset

source "config.sh"    || exit 1
source "functions.sh" || exit 1

PN="$(basename "$0")"
# l - log file 
# o - возраст хранения архивов
# d - каталог назначения 
# h - справка
OPTIONS=":l:o:d:h"
TAR="$(which tar)"

Usage () {
echo "$PN - архивирование каталогов.
    Порядок использования: $PN [-h] | [-l FILE] [-d DIR] [-o NUM_DAYS] srcdir1 ... srcdirN
        -h  вызов справки
        -l  лог файл        
        -d  каталог назначения архивов
        -o  удалять копии старше указанного количества дней
        srcdir исходный каталог"
}

while getopts ${OPTIONS} argument
do
	case "${argument}" in
	h)
		Usage
		exit 0
		;;
	l)
		LOG_FILE="${OPTARG}"		
		;;
	o)
		ARCH_DAYS="${OPTARG}"		
		;;
	d)
		DST_DIR="${OPTARG}"		
		;;
	\?)
		echo "Опция -${OPTARG} не найдена"
		exit 1
		;;
	:)
		echo "Не указан аргумент для опции -{OPTARG}"
		exit 1
		;;
	esac
done

if [ $OPTIND -gt 1 ]; then
	shift $((${OPTIND} - 1))
fi

# Настрока лог файла и перенаправления в лог файл
if [ -n "${LOG_FILE:+x}" ]; then
	Check_Dir "$(dirname ${LOG_FILE})"

	if [ -e "${LOG_FILE}" -a ! -w "${LOG_FILE}" ]; then
		echo "Невозможно записать в файл ${LOG_FILE}."
		exit $ERR_BAD_FILE
	fi		

	exec 3>&1 4>&2
	exec &>>"${LOG_FILE}"
fi

if [ $# -lt 1 ]; then 
	echo "Не указаны каталоги для архивирования."
	Usage
	exit $ERR_NUM_ARGS
fi

Check_Dir "${DST_DIR}"
Check_Number "${ARCH_DAYS}"

# в цикле происходит архивация всех переданных каталогов
while (( $# ))
do
	TMP_SRC="$1"
	Print_Stamp "Архивация ${TMP_SRC}"
	DATE_STAMP="$(date '+%d%m%Y-%H_%M')"
	if [ -d "${TMP_SRC}" ]; then
		NAME="$(basename ${TMP_SRC})"
		SRC_PREFIX_DIR="$(dirname ${TMP_SRC})"
		DST_NAME="${DST_DIR}/${NAME}-${DATE_STAMP}.tar.gz"
		${TAR} -C "${SRC_PREFIX_DIR}" -czf "${DST_NAME}" "${NAME}"
		if [ $? -ne 0 ]; then
			Report "Произошла ошибка при создании архива ${DST_NAME}."
		else			
			Report "Архив ${DST_NAME} успешно создан."
			# Remove old
			Remove_Old "$ARCH_DAYS" "${DST_DIR}" "${NAME}-*.tar.gz"		
		fi	
	else	
		Report "Каталог ${TMP_SRC} не найден. Архивация не выполнена."
	fi

	shift
done

#Восстановление потоков stdout stderr
if [ -n "${LOG_FILE:+x}" ]; then
	exec 1>&3 3>&-
	exec 2>&4 4>&-
fi

exit 0
