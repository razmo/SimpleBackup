#!/bin/bash
# Инкрементальный бэкап с использованием утилиты rsync

# немедленно завершать работу в случае возникновения ошибки
#set -o errexit
# если переменная не определена генерировать ошибку
set -o nounset
# возвращать ненулевой код завершения, если в конвейере произошла ошибка 
#set -o pipefail

PN="$(basename "$0")"
#PROGDIR="$(basepath "$0")"
OPTIONS=":hu:p:e:o:"
FIND="$(which find)"
RSYNC="$(which rsync)" 

source "functions.sh" || exit 1
source "config.sh"    || exit 1

Usage () {
    echo "$PN - инкрементальный бэкап с использованием утилиты rsync.
    Порядок использования: $PN [-h] | [-e FILE] [-u NAME -p FILE] [-o NUM_DAYS] srcdir dstdir
        -h  вызов справки
        -e  файл, содержащий исключения, которые не попадут в бэкап
        -u  имя пользователя при аутентификации к удаленному ресурсу
        -p  файл, содержащий пароль
        -o  удалять копии старше указанного количества дней
        srcdir исходный каталог
        dstdir каталог назначения"
}

if [ ! -x "${RSYNC}" ]; then
    echo "Программа rsync не установлена."
    exit $ERR_NOT_FOUND
fi

if [ $# -lt 1 ]; then
    Usage
    exit $ERR_NUM_ARGS
fi

# Обработка необязательных аргументов 
while getopts ${OPTIONS} argument
do
    case "${argument}" in
        h)
            Usage
            exit 0
            ;;
        e)
            EXCLUDE_FILE="${OPTARG}"
	        #Check_File "${EXCLUDE_FILE}"
            ;;
        p)
            PASSWORD_FILE="${OPTARG}"
            Check_File "${PASSWORD_FILE}"
            ;;
        u)
            AUTH_USER="${OPTARG}"
            #if [ -z "$AUTH_USER" ]; then
            #    echo "Неверное имя пользователя ${AUTH_USER}."
            #    exit $ERR_BAD_OPTION
            #fi
            #AUTH_USER="${AUTH_USER}@"
            ;;
        o)
            DAYS="${OPTARG}"
            Check_Number "${DAYS}"
            ;;
        \?)
            echo "Опция -${OPTARG} не найдена."
            Usage
            exit $ERR_BAD_OPTION
            ;;
        :)
            echo "Не указан аргумент для опции -${OPTARG}."
            Usage
            exit $ERR_BAD_OPTION_ARG
            ;;
    esac  
done

#Обработка позиционных параметров srcdir dstdir
if [ $# -lt 2 ]; then
    Usage
    exit $ERR_NUM_ARGS
fi

# если OPTIND > 1 значит был передан хотя бы один параметр и getops обработала его,
# изменив переменную OPTIND, которая теперь содержит позицию следующего аргумента 

if [ $OPTIND -gt 1 ]; then
    shift $((${OPTIND} - 1))
fi

readonly SRCDIR=$1
#Check_Dir "$SRCDIR"
shift

readonly DSTDIR=$1
Check_Dir "$DSTDIR"

# Настройка остальных параметров
readonly DATETIMESTAMP="$(date '+%d%m%Y_%H-%M')"
readonly BACKUP_PATH="${DSTDIR}/${DATETIMESTAMP}"
readonly LATEST_LINK_PATH="${DSTDIR}/latest"

# При первом запуске ссылки LATEST_LINK_PATH на последний инкрементальный архив еще е существует,
# rsync на это ругнется. При последующих запусках rsync, ссылаясь на предыдущий бэкап в
# LATEST_LINK_PATH, будет производить синхронизацию в каталог BACKUP_PATH следующим образом:
# - новый или измененный файл будет скопирован полностью
# - файл, который был удален, по прежнему останется в папке, на которую ссылается LATEST_LINK_PATH
# - на все остальные файлы будет создана жесткая ссылка в каталоге BACKUP_PATH
# ls -l покажет увеличение жестких ссылок на 1
: <<COMMENT_BLOCK
rsync -avx --delete --delete-excluded \
    --exclude-from=""
    "${SRCDIR}/"   \
    --link-dest "${LATEST_LINK_PATH}" \
    "${BACKUP_PATH}"
COMMENT_BLOCK

if [ -n "${EXCLUDE_FILE:+x}" ]; then
    EXCLUDED="--delete-excluded --exclude-from=$(printf '%q ' "${EXCLUDE_FILE}")"
else
    EXCLUDED=""
fi

if [ -n "${AUTH_USER:+x}" -a -n "${PASSWORD_FILE:+x}" ]; then
    AUTHENTICATION="--password-file=$(printf '%q ' "${PASSWORD_FILE}")"
    AUTH_USER="${AUTH_USER}@"
else
    AUTHENTICATION=""
    AUTH_USER=""
fi

Print_Stamp "Начало синхронизации."

eval "${RSYNC}" "-avxz" "--delete" "${EXCLUDED}" "${AUTHENTICATION}" \
 "$(printf '%q ' "${AUTH_USER}${SRCDIR}/")" \
 "--link-dest" \
 "$(printf '%q ' "${LATEST_LINK_PATH}" "${BACKUP_PATH}")"

if [ $? -ne 0 ]; then
    echo "Произошла ршибка при резервном копировании каталога ${SRCDIR}. Удалям каталог ${BACKUP_PATH}. Пропускаем чистку старых резервных копий."
    rm -rf "${BACKUP_PATH}"
    Report "Резервное копирование каталога <b>${SRCDIR}</b> не выполнено."
    exit ${ERR_EXECUTE}
fi

Report "Резервное копирование каталога <b>${SRCDIR}</b> выполнено <i>успешно</i>."


# Обновляем символическую ссылку на последний бэкап
rm -rf "${LATEST_LINK_PATH}"
ln -s "${BACKUP_PATH}" "${LATEST_LINK_PATH}"

Print_Stamp "конец синхронизации."

# Чистим старые резервные копии 
Remove_Old ${DAYS} "${DSTDIR}" "*"

exit 0

# за основу был взят пример из статьи https://linuxconfig.org/how-to-create-incremental-backups-using-rsync-on-linux
