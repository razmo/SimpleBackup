readonly ERR_EXECUTE=1
readonly ERR_NUM_ARGS=5
readonly ERR_BAD_OPTION=6
readonly ERR_BAD_OPTION_ARG=7
readonly ERR_BAD_FILE=8
readonly ERR_NOT_FOUND=9

Check_Dir () {
    if [[ ! -d "$1" || ! -w "$1" ]]; then 
        echo "Директория ${1} не найдена или не доступна для записи."
        exit $ERR_BAD_FILE
    fi
}

Check_File () {    
    if [ ! -r "$1" ]; then
        echo "Невозможно прочитать файл ${1}."
        exit $ERR_BAD_FILE
    fi
}

Print_Stamp () {
    echo "----- $1 -----"
    echo "----- $(date '+%d %m %Y %H:%M:%S') -----"    
}

Check_Number () {
    if [[ ! "${1}" =~ ^[[:digit:]]+$ ]]; then
        echo "Укажите количество дней хранения резервных копий."
        exit $ERR_BAD_OPTION
    fi
}

CURL="$(which curl)"

if [ $? -eq 1 ]; then
    echo "Команда curl не установлена."
    exit $ERR_NOT_FOUND
fi

function SendTgMessage () {
    MESSAGE="$(</dev/stdin)"

    # проверка параметров для отправки
    #[ -z ${RECIEVERID:+x} -o -z ${TGKEY:+x} -o -z ${MESSAGE:+x} ] && exit 10
    : ${RECIEVERID:?Укажите ID получателя} ${TGBOTKEY:?Укажите API KEY для доступа к боту} ${MESSAGE:?Укажите текст сообщения}

    echo "Выполняется отправка сообщения получателю: ${RECIEVERID}"
    if [ -z "${PROXY:+x}" ]; then
        PROXY=""
    else
        PROXY="--proxy ${PROXY}"
    fi

    ANSWER=$($CURL --header 'Content-Type: application/json' ${PROXY} \		
                --request 'POST' \
                --data "{\"chat_id\":\"${RECIEVERID}\",\"text\":\"${MESSAGE}\",\"parse_mode\":\"HTML\"}" "https://api.telegram.org/bot${TGBOTKEY}/sendMessage" ) &>/dev/null

    _TMP=${ANSWER[@]:6:4}

    if [ ! "${_TMP,,}" = "true" ]; then
        echo "Ошибка отправки сообщения: $ANSWER"
        exit $ERR_EXECUTE
    fi
}

FIND="$(which find)"

# Дублирует сообщение в лог файл или в stdout
Report () {
    echo "$1" 
    echo "$1" | SendTgMessage
}

Remove_Old () {
    local days="$1"
    local dir="$2"
    local name="$3"

    Print_Stamp "Удаление резервных копий старше ${days} дней."
	
    ${FIND} "${dir}" -maxdepth 1 -mtime "+${days}" -iname "${name}" -print0 | xargs -I arch -0 /bin/rm -rfv "arch"
	if [ $? -ne 0 ]; then
        Report "Произошла ошибка при удалении файлов из <b>${dir}</b> старше <b>${days}</b> дней."
        Print_Stamp "Не удалось удалить старые резервные копии."    
        exit $ERR_EXECUTE
    fi
    Print_Stamp "Удаление резервных копий завершено."
}

